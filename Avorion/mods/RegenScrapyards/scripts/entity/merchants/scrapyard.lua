package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/entity/ai/?.lua"
package.path = package.path .. ";mods/RegenScrapyards/config/?.lua"

require ("galaxy")
require ("utility")
require ("faction")
require ("randomext")
require ("callable")
require ("stringutility")

require ("shipgenerator")

SectorGenerator = require ("SectorGenerator")
local Config = require("RSYConfig")


Scrapyard.initializeRSYParent = Scrapyard.initialize
function Scrapyard.initialize()
	Scrapyard.initializeRSYParent()
	
	-- Register callback
	if onServer() then
		local faction = Faction()
		if faction and faction.isAIFaction then
			Sector():registerCallback("onRestoredFromDisk", "onRestoredFromDisk")
		end
	end
end


function Scrapyard.onRestoredFromDisk(timeSinceLastSimulation)
	Config.log(2, "Restore from disk")
	
	-- Add timer!!
	--if Sector().numPlayers == 0 then
		
		local resourcesTotal = Scrapyard.getWreckageResources()
		
		local x, y = Sector():getCoordinates()
		local shipVolume = Balancing_GetSectorShipVolume(x, y)
		local initialResources = shipVolume * 1000 -- roughly expected total resources of that scrapyard
		
		local minResources = initialResources / 4 -- When more then 75% of the wreckages are salvaged, generate some new
		
		Config.log(3, "Initial resources:", math.floor(initialResources / 1000) / 1000, "mio")
		Config.log(3, "Regenerate threshold:", math.floor(minResources / 1000) / 1000, "mio")
		
		if resourcesTotal < minResources then
			Scrapyard.regenerateWreckages()
		end
		
		
		if not Scrapyard.hasDefender() then
			Scrapyard.regenerateDefender()
		end
	--end
end

-- Get total amount of wreckage resources in the sector
function Scrapyard.getWreckageResources()
	local resourcesTotal = 0
	local timer = nil
	if Config.logLevel > 2 then
		timer = HighResolutionTimer()
		timer:start()
	end
	
	for _,entity in pairs({Sector():getEntitiesByType(EntityType.Wreckage)}) do
		entity:waitUntilAsyncWorkFinished()
		local resources = Scrapyard.getTotal({entity:getMineableResources()})
		
		if resources == nil or resources <= 10 then
			entity:setPlan(BlockPlan())
		else
			resourcesTotal = resourcesTotal + resources
		end
	end
	
	if Config.logLevel > 2 then
		timer:stop()
		Config.log(3, "getWreckageResources execution time", timer.milliseconds, "ms")
		timer:reset()
	end
	
	Config.log(2, "Total resources", math.floor(resourcesTotal / 1000) / 1000, "mio")
	
	return resourcesTotal
end

-- Check if a defender is present
function Scrapyard.hasDefender()
	defender = false
	for _,entity in pairs({Sector():getEntitiesByFaction(Faction().index)}) do
		if entity.isShip and entity:hasScript("patrol.lua") then
			defender = true
			break
		end
	end
	
	return defender
end

-- Regenerate wreckages
function Scrapyard.regenerateWreckages()
	Config.log(3, "Regenerate wreckages")
	if Config.logLevel > 2 then
		timer = HighResolutionTimer()
		timer:start()
	end
	
	local x, y = Sector():getCoordinates()
	local generator = SectorGenerator(x, y)

    for i = 0, math.random(150, 300) do
        generator:createWreckage(Faction());
    end
	
	if Config.logLevel > 2 then
		timer:stop()
		Config.log(3, "regenerateWreckages execution time", timer.milliseconds, "ms")
		timer:reset()
	end
end

-- Regenerate defender(s)
function Scrapyard.regenerateDefender()
	Config.log(3, "Regenerate defender")
	local x, y = Sector():getCoordinates()
	local generator = SectorGenerator(x, y)
	local defenders = math.random(1, 3)
    for i = 1, defenders do
        ShipGenerator.createDefender(Faction(), generator:getPositionInSector())
    end
end


function Scrapyard.getTotal(list)
    local total = 0
    for _,k in pairs(list) do
        total = total + k
    end
    return total
end



