package.path = package.path .. ";data/scripts/lib/?.lua"


local Config = {}

require ("utility")

Config.Author = "Hammelpilaw"
Config.ModName = "RegenScrapyards"
Config.version = {
    major=0, minor=1, patch = 0,
    string = function()
        return  Config.version.major .. '.' ..
                Config.version.minor .. '.' ..
                Config.version.patch
    end
}

-- Custom configurations
Config.Settings = {
	
}



Config.logLevel = 1
if GameSettings().devMode and Config.logLevel < 4 then
	Config.logLevel = 4
end

Config.prefix = "["..Config.ModName.." - "..Config.version.string().."]"
Config.log = function(logLevel, var, ...) 
	if logLevel <= Config.logLevel then
		if type(var) == "table" then
            printTable(var, Config.prefix .. " ")
        else
			if var == nil then var = "nil" end
			print(Config.prefix, var, ...)
		end
	end
end

return Config