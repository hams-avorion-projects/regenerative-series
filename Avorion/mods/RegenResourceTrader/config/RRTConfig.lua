package.path = package.path .. ";data/scripts/lib/?.lua"


local Config = {}

require ("utility")

Config.Author = "Hammelpilaw"
Config.ModName = "RegenResourceTrader"
Config.version = {
    major=0, minor=1, patch = 0,
    string = function()
        return  Config.version.major .. '.' ..
                Config.version.minor .. '.' ..
                Config.version.patch
    end
}

-- Custom configurations
Config.Settings = {
	initResMulti		= 1,		-- If higher then 1 multiply initial resource amount by this
	initResMultiMobile	= 1,		-- If higher then 1 multiply initial resource of mobile resource trader amount by this
	restoreAmount		= 1,		-- Multiplier for restore steps (already multiplied by initResMulti)
	restoreDelay		= 6,		-- Delay for restore steps (hours - set to "X / 60" to set it to X minutes)
	maxMaterial			= 1000000,	-- Max amount per material
}


Config.Settings.restoreDelay = Config.Settings.restoreDelay * 60 * 60 -- Change from minutes to seconds


Config.logLevel = 1
if GameSettings().devMode and Config.logLevel < 4 then
	Config.logLevel = 4
	Config.Settings.restoreDelay = 15 -- 15 seconds in dev mode
end

Config.prefix = "["..Config.ModName.." - "..Config.version.string().."]"
Config.log = function(logLevel, var, ...) 
	if logLevel <= Config.logLevel then
		if type(var) == "table" then
            printTable(var, Config.prefix .. " ")
        else
			if var == nil then var = "nil" end
			print(Config.prefix, var, ...)
		end
	end
end

return Config