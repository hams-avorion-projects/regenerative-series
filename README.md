# Regenerative Series

The regenerative series contains mods to regenerate things in Avorion or improve the vanilla regeneration of things.

All mods are in this series are independent from each other, so you can use one or more of them.

It contains the following mods:

* Regenerative Resource Trader
* Regenerative Scrapyard


## Regenerative Resource Trader

This mod makes resource trader more useful. In the early game buying resources is too expensive most times. In the later game the low amount you can buy is not worthy to buy it.

With this mod resource traders regenerate materials over time up to a maximum limit.

### Insert code
###### File:

`data/scripts/entity/merchants/resourcetrader.lua`

###### Code:

    local status, RRT = pcall(require, 'mods.RegenResourceTrader.scripts.entity.merchants.resourcetrader')
    if not status then print('Mod: RegenResourceTrader - failed to extend resourcetrader.lua!') print(RRT) end



## Regenerative Scrapyard
Regenerate wreckages at scrapyards.

### Insert code
###### File:

`data/scripts/entity/merchants/scrapyard.lua`

###### Code:

    local status, RSY = pcall(require, 'mods.RegenScrapyards.scripts.entity.merchants.scrapyard')
    if not status then print('Mod: RegenScrapyards - failed to extend scrapyard.lua!') print(RSY) end


## Mod installation
Open folder `Avorion/mods` of this package and copy the mods you wish to install to your local game folder: `steamapps/common/Avorion/mods/`. If the folder `mods/` does not exist, create it.

Do this for all mods you wish to install:  
Watch the `Insert code` section of the mod: open the specified file and insert the code to the very bottom.
